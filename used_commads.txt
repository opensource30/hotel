composer require symfony/orm-pack
composer require --dev symfony/maker-bundle
php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console make:controller BookingController
composer require doctrine/annotations
composer require symfony/twig-bundle
composer require symfony/form
composer require --dev phpunit/phpunit symfony/test-pack