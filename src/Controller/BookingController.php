<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Booking;


/* this class is used for create booking, check availability of room ,showing booking details 



*/
class BookingController extends AbstractController
{   
    
	/*showing list of bookings and calendar and user can give booking from this page */
	public function index(): Response
    {
  
		$booking = $this->getDoctrine()
            ->getRepository(Booking::class)
            ->findAll();
		
        $contents = $this->renderView('index.html.twig', [
            'booking_list' => $booking,
			
        ]);

        return new Response($contents);

		
    }
	/*Showing a details of booking */
	public function show(int $id): Response
    {
        $booking = $this->getDoctrine()
            ->getRepository(Booking::class)
            ->find($id);

        if (!$booking) {
            throw $this->createNotFoundException(
                'No booking found for id '.$id);
        }

       // return new Response('Booking Details : '.$booking->getFirstName().' '.$booking->getLastName());
        $contents = $this->renderView('show.html.twig', [
            'booking' => $booking,
        ]);

        return new Response($contents);
      
    }
	
	
	/*user is booking a room and it redirected to home page. 
	User can book room from while he is looking a specific booking details */
	public function createBooking(Request $request): Response
    {
    

		if ($request->isMethod('POST')) {
		
				$entityManager = $this->getDoctrine()->getManager();

					$booking = new Booking();
					$booking->setStartDate(new \DateTime($request->request->get('_ckin'))); 

					$booking->setEndDate(new \DateTime($request->request->get('_ckout')));
					$booking->setRoomNumber('A1');
					$booking->setFirstName($request->request->get('_first_name'));
					$booking->setLastName($request->request->get('_last_name'));
					$booking->setEmail($request->request->get('_email'));
					$booking->setCreatedAt(new \DateTime());
					// tell Doctrine you want to (eventually) save the Booking (no queries yet)
					$entityManager->persist($booking);

					// actually executes the queries (i.e. the INSERT query)
					$entityManager->flush();
				return $this->redirectToRoute('home');
		}
		
		
		return new Response('Saved new booking with id ');
    }


/*	  Raw Query to check overlapping
		$sql = "SELECT count(id) from booking where 
		'2021-12-23' between start_date  and end_date - INTERVAL '1 DAY' OR
		'2021-12-25'  between start_date+ INTERVAL '1 DAY' and end_date OR 
		start_date BETWEEN '2021-12-23' AND '2021-12-24'
            ";
     
*/
	/*checking whether slected date is overlapping with existing reserved date. 
	The day some one checkout, another person can checkin in same date
	it also consider single room
	*/
	public function check(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
		
			$checkin = $request->request->get('checkin');
		    $checkout=$request->request->get('checkout');

			  $query = $em->createQuery('SELECT u FROM App\Entity\Booking u where 
			(u.start_date <= :checkin and :checkout < u.end_date )
			 OR (:checkin > u.start_date and :checkout <= u.end_date) OR 
			(u.start_date >= :checkin AND u.start_date < :checkout)')
			->setParameter('checkin', $checkin)
			->setParameter('checkout',$checkout);

			  $bookings = $query->getResult();
			  //count is booolean value to check whether overlapping happened
			  if (!$bookings){
				  
				  $count=0;
			  }
			  else {
				  
				  $count=1;
			  }
				  
	  return new Response($count);
    }
	/* list down all dates when the room is booked, it is used for calednar showing using ajax call  */
	public function dateList(): Response
    {
        $booking = $this->getDoctrine()
            ->getRepository(Booking::class)
            ->findAll();
		$arr=[];
		$arr_end =[];
		$count =0;
		$i=0;
		$final_arr =[];
		foreach ($booking as $item) {
           $arr[$count] =$item->start_date->format('Y-m-d');
		   $arr_end[$count] =$item->end_date->format('Y-m-d');
		   $temp_date = $arr[$count];
		  
		   while($temp_date <= $arr_end[$count]){
			   $final_arr[$i] = $temp_date;
			   $temp_date = date('Y-m-d', strtotime("+1 day", strtotime($temp_date)));
			   $i = $i+1;
		   }
		    $count = $count + 1;
         }
      
         $ar = implode(",",$final_arr);
				  
	  return new Response($ar);
    }
	
	
}


	